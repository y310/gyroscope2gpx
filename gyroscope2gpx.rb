require 'csv'
require 'time'
require 'erb'
require 'pry'

class Track
  attr_accessor :start_time, :end_time, :points, :track_points

  def initialize(row)
    @start_time, @end_time, _, @points, _ = row
    @start_time = Time.parse(@start_time + ' UTC')
    @end_time = Time.parse(@end_time + ' UTC')
    @points = points.scan(/\((\d+\.\d+), (\d+\.\d+)\)/).map {|l| l.map(&:to_f) }
    @delta = @points.size > 1 ? (@end_time - @start_time).to_f / (@points.size - 1) : 0
    @track_points = @points.map.with_index do |point, i|
      {
        lat: point[0],
        lon: point[1],
        time: @start_time + @delta * i,
      }
    end
  end
end

begin_date = Time.parse(ARGV[1] + ' 00:00:00 UTC') if ARGV[1]
logs = CSV.read(ARGV[0])[1..-1].map {|l| Track.new(l) }
tracks = logs.flat_map(&:track_points).sort_by {|t| t[:time] }
tracks = tracks.select {|t| t[:time] >= begin_date } if begin_date
template = <<-ERB
<?xml version="1.0" encoding="utf-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1">
  <trk>
    <trkseg>
      <%- tracks.each do |track| -%>
      <trkpt lat="<%= track[:lat] %>" lon="<%= track[:lon] %>">
        <time><%= track[:time].strftime('%Y-%m-%dT%H:%M:%SZ') %></time>
      </trkpt>
      <%- end -%>
    </trkseg>
  </trk>
</gpx>
ERB
puts xml = ERB.new(template, nil, '-').result(binding)
