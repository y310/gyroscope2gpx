# gyroscope2gpx

Converter for the gyroscope app travels data exported from https://gyrosco.pe/export/

## Usage

```sh
ruby gyroscope2gpx.rb EXPORTED_FILE BEGIN_DATE > track.gpx
```
